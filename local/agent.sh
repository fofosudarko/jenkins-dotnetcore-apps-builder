#!/bin/bash
#
# File: agent.sh -> builds and deploys dotnetcore apps onto local server
#
# (c) 2019 Frederick Ofosu-Darko <fofosudarko@gmail.com, fofosudarko@aesensainnova.com>
#
# Usage: bash agent.sh APPLICATION:PORT:DESCRIPTION APP_ENVIRONMENT APP_TYPE BUILD_TYPE JENKINS_JOB  DEPENDENCIES
#
#

## - start here

runAs ()
{
  sudo su $DOTNETCOREUSER -c "$1"
}

getAppService ()
{
  runAs cat <<EOF
[Unit]
Description=${APP_DESCRIPTION}

[Service]
WorkingDirectory=${BUILD_DIR}
ExecStart=${DOTNET} ${BUILD_DIR}/${APP}.dll --server.urls "http://*:${APP_PORT};"
Restart=always
RestartSec=10
User=${DOTNETCORE_USER}
SyslogIdentifier=${JENKINS_JOB}
Environment=ASPNETCORE_ENVIRONMENT=${APP_ENVIRONMENT}
Environment=DOTNET_PRINT_TELEMETRY_MESSAGE=false
StandardOutput=file:${LOG_FILE}
StandardError=file:${LOG_FILE}

[Install]
WantedBy=multi-user.target
EOF
}

writeAppService ()
{
  cat $STDIN | runAs "tee $APP_SERVICE"
}

buildApp ()
{
  local app_project_file="$SRC_DIR/$APP.csproj"
  
  echo Discarding contents of $SRC_DIR ...
  
  runAs "rm -rf $SRC_DIR/*"

  echo Copying to $SRC_DIR ...
  
  runAs "cp -ap $JENKINS_JOB_DIR/* $SRC_DIR"

  echo Changing to $SRC_DIR ...
  
  cd "$SRC_DIR"

  if [[ -n "$DEPENDENCIES" ]]
  then
    addDependencies "$app_project_file" "$DEPENDENCIES"
  fi

  runAs "$DOTNET publish -c ${BUILD_TYPE} -r ${PLATFORM} -f $APP_FRAMEWORK -o $BUILD_DIR"
}

addDependencies ()
{
  local parent_project_file="$1" dependencies="$2"

  for dependency in $dependencies
  do
    jenkins_job=$(splitTo "$dependency" ':' '1')
    application=$(splitTo "$dependency" ':' '2')

    src_dir="$APPS_DIR/${jenkins_job}/src"
    project_file="$src_dir/$application.csproj"

    if ! grep -qP "$application.csproj" "$parent_project_file"
    then
      runAs "$DOTNET add reference $project_file"
    fi

  done
}

createDirectories ()
{
  for dir in $APPS_DIR $SRC_DIR $BUILD_DIR $LOG_DIR
  do
    [[ ! -d "$dir" ]] && runAs "mkdir -vp $dir"
  done
}

splitTo ()
{
  local string="$1"
  local delimiter="$2"
  local position="$3"

  echo -ne "$string" | awk -F"$delimiter" "{ print $"$position"; }"
}

runConsoleApp ()
{
  runAs "$DOTNET $BUILD_DIR/$APP.dll"
}

runWebApiApp ()
{
  if [[ -n "$APP_PORT" ]]
  then
    if [[ ! -s "$SYSTEMD_SERVICE" ]]
    then
      getAppService | writeAppService

      runAs "cp -ap $APP_SERVICE $SYSTEMD_SERVICE"

      sudo systemctl reset-failed
      sudo systemctl daemon-reload
      sudo systemctl enable $SERVICE_UNIT
      sudo systemctl start $SERVICE_UNIT
    else
      sudo systemctl restart $SERVICE_UNIT
    fi
  fi

  sudo systemctl status $SERVICE_UNIT
}

COMMAND=$0

if [[ $# < 5 ]]
then
  echo "$COMMAND: expects 5 or more arguments i.e. APPLICATION:PORT:DESCRIPTION APP_ENVIRONMENT APP_TYPE BUILD_TYPE JENKINS_JOB DEPENDENCIES"
  exit 1
fi

APPLICATION=$1
shift

APP_ENVIRONMENT=$1
shift 

APP_TYPE=$1
shift 

BUILD_TYPE=$1
shift 

JENKINS_JOB=$1
shift

DEPENDENCIES="$@"

: ${DOTNET=$(which dotnet)}
: ${PLATFORM='linux-x64'}
: ${DOTNETCORE_USER='dotnetcore'}
: ${DOTNETCORE_HOME='/var/lib/dotnetcore'}
: ${JENKINS_WORKSPACE='/var/lib/jenkins/workspace'}
: ${JENKINS_JOB_DIR="${JENKINS_WORKSPACE}/${JENKINS_JOB}"}
: ${APPS_DIR="${DOTNETCORE_HOME}/apps"}
: ${APP_DIR="${APPS_DIR}/$JENKINS_JOB"}
: ${SRC_DIR="$APP_DIR/src"}
: ${BUILD_DIR="$APP_DIR/build"}
: ${APP_FRAMEWORK='netcoreapp2.1'}
: ${LOG_DIR="${DOTNETCORE_HOME}/logs"}
: ${LOG_FILE="${LOG_DIR}/${JENKINS_JOB}.log"}
: ${SERVICE_UNIT="${JENKINS_JOB}.service"}
: ${APP_SERVICE="${APP_DIR}/${SERVICE_UNIT}"}
: ${SYSTEMD_SERVICE="/etc/systemd/system/${SERVICE_UNIT}"}

APP=$(splitTo "$APPLICATION" ':' '1')
APP_PORT=$(splitTo "$APPLICATION" ':' '2')
APP_DESCRIPTION=$(splitTo "$APPLICATION" ':' '3')

if ! echo -ne "$APP_TYPE" | grep -qP "^(console|webapi)$"
then
  echo "$COMMAND: Invalid application type. Should be console or webapi"
  exit 1
fi

if ! echo -ne "$APP_ENVIRONMENT" | grep -qiP "^(staging|production)$"
then
  echo "$COMMAND: Invalid application environment. Should be staging or production"
  exit 1
fi

if ! echo -ne "$BUILD_TYPE" | grep -qP "^(debug|release)$"
then
  echo "$COMMAND: Invalid build type. Should be debug or release"
  exit 1
fi

createDirectories

buildApp

case "$APP_TYPE"
in
  console)
    runConsoleApp
  ;;
  webapi)
    runWebApiApp
  ;;
esac

exit 0

## -- finish
